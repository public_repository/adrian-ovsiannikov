from flask_login import UserMixin
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, Integer, String, ForeignKey, JSON, Boolean
from sqlalchemy.orm import relationship

db = SQLAlchemy()


class Field(db.Model):    # type: ignore
    game_id = Column(Integer, ForeignKey('game.id'), primary_key=True)
    player_id = Column(Integer, ForeignKey('user.id'), primary_key=True)
    data = Column(JSON)
    its_turn = Column(Boolean, default=False)

    game = relationship('Game', backref='fields', foreign_keys=[game_id])
    player = relationship('User', backref='fields', foreign_keys=[player_id])


class User(UserMixin, db.Model):    # type: ignore
    id = Column(Integer, primary_key=True)
    username = Column(String(30), unique=True, nullable=False)
    password = Column(String(256), nullable=False)

    fields: list[Field]


class Game(db.Model):    # type: ignore
    id = Column(Integer, primary_key=True)
    status = Column(String(256))
    num_tanks = Column(Integer, nullable=False)

    fields: list[Field]
