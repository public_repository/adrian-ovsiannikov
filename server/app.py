from flask import Flask
from flask_restful import Api
from flask_cors import CORS
from flask_socketio import SocketIO
from flask_login import LoginManager
from os import getenv

from models import db, User
from api.AuthorizationHandler import AuthorizationHandler
from api.SignUpHandler import SignUpHandler
from api.HistoryHandler import HistoryHandler
from api.GameHandler import GameHandler

URL_API_AUTH = '/api/authorization'
URL_API_SIGNUP = '/api/sign-up'
URL_API_HISTORY = '/api/history'


class App(Flask):
    resources = {
        URL_API_AUTH: AuthorizationHandler,
        URL_API_SIGNUP: SignUpHandler,
        URL_API_HISTORY: HistoryHandler,
    }

    def __init__(self, db_url=getenv('DB_URL')):
        Flask.__init__(self, __name__)

        self.config['SECRET_KEY'] = getenv('SECRET_KEY')
        self.config['SQLALCHEMY_DATABASE_URI'] = db_url
        self.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

        db.init_app(self)

        CORS(self, supports_credentials=True)

        login_manager = LoginManager()
        login_manager.init_app(self)
        login_manager.user_loader(lambda uid: User.query.get(int(uid)))

        api = Api(self)
        for url, handler in self.resources.items():
            api.add_resource(handler, url)

        self.socket = SocketIO(self, cors_allowed_origins="*")
        self.socket.on_namespace(GameHandler('/'))


def run_app():
    app = App()
    app.socket.run(app, port=5000, host="0.0.0.0", debug=True)


if __name__ == '__main__':
    run_app()
