import re
from os import getenv
import pytest
from app import App, db
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from sqlalchemy import create_engine, exc, MetaData

DB_URL = getenv('DB_URL')


@pytest.fixture(scope='session')
def database():
    """
    Create a Postgres database for the tests,
    and drop it when the tests are done.

    return postgres URI
    """
    dbname = 'test_db'
    engine = create_engine(DB_URL)
    engine.raw_connection().set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    with engine.connect() as conn:
        try:
            conn.execute(f'create database {dbname};')
        except exc.ProgrammingError:
            metadata = MetaData()
            metadata.drop_all(engine)

        test_db_url = re.sub(r'\b[\w-]+?\Z', dbname, DB_URL, count=1)
        yield test_db_url

        conn.execute(
            f'alter database "{dbname}" with allow_connections false;')
        conn.execute(
            'select pg_terminate_backend(pg_stat_activity.pid)'
            'from pg_stat_activity where pg_stat_activity.datname = %s;',
            (dbname,),
        )
        conn.execute(f'drop database if exists {dbname};')


@pytest.fixture
def app(database):
    app = App(db_url=database)
    app.config['TESTING'] = True
    db.create_all(app=app)
    yield app
    db.drop_all(app=app)


@pytest.fixture
def client(app):
    with app.test_client() as client:
        with app.app_context():
            yield client


@pytest.fixture
def socket(app, client):
    with app.socket.test_client(app, flask_test_client=client) as socket:
        yield socket
