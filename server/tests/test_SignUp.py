from random import randint
from models import User
from werkzeug.security import check_password_hash


def sign_up(client, username, password):
    return client.post(
        '/api/sign-up',
        json={
            "username": username,
            "password": password
        }
    )


def test_signup_post(client):
    num = randint(0, 100)
    username = f'user{num}'
    password = f'password{num}'
    response = sign_up(client, username, password)
    assert response.status_code == 200
    user = User.query.filter_by(username=username).first()
    assert user
    assert check_password_hash(user.password, password)

    response = client.post('/api/sign-up', json={
        "username": username,
        "password": password
    })
    assert response.status_code != 200
