import pytest
from app import db, URL_API_AUTH
from models import User
from werkzeug.security import generate_password_hash
from time import monotonic_ns


def create_user(username, password):
    user = User(
        username=username,
        password=generate_password_hash(password, method='sha256')
    )
    db.session.add(user)
    db.session.commit()
    return user


@pytest.fixture
def user():
    num = monotonic_ns()
    username = f'user{num}'
    password = f'password{num}'
    user = create_user(username, password)
    db.session.add(user)
    db.session.commit()
    yield user, username, password
    db.session.delete(user)
    db.session.commit()


def log_in(client, username, password, remember):
    return client.post(
        URL_API_AUTH,
        json={
            "username": username,
            "password": password,
            "remember": remember
        }
    )


def logout(client):
    return client.delete(URL_API_AUTH, json={})


def check_auth(client):
    return client.get(URL_API_AUTH, json={})


def test_auth_post(client, user):
    user, username, password = user

    response = log_in(client, username, password, True)
    assert response.status_code == 200
    assert response.headers.get('Set-Cookie')

    response = client.get(URL_API_AUTH)
    assert response.status_code == 200
    assert response.json
