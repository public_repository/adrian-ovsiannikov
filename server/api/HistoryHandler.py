from flask_restful import Resource
from flask_login import login_required, current_user

from api.GameHandler import get_opponent, get_game_score


class HistoryHandler(Resource):

    @login_required
    def get(self):
        fields = current_user.fields
        games = []
        for f in fields:
            game = f.game
            opponent = get_opponent(game.id, current_user.id)
            game_score = get_game_score(game)
            winner_id = [
                pid for pid, score in game_score.items()
                if score >= game.num_tanks
            ]
            if winner_id:
                if winner_id[0] == current_user.id:
                    status = 'Victory'
                else:
                    status = 'Defeat'
            else:
                status = 'Interrupted'

            score = f'{game_score.get(current_user.id, 0)} : ' \
                    f'{game_score.get(opponent.id, 0)}'

            if opponent:
                opponent = opponent.username
            else:
                opponent = ''

            games.append([game.id, status, score, opponent])

        games.sort(key=lambda g: int(g[0]), reverse=True)
        return {"headers": ["Id",
                            "Status",
                            "Score (Your:Opponent)",
                            "Opponent"],
                "data": games
                }, 200
