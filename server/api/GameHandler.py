from flask_login import login_required, current_user
from flask_socketio import send, emit, join_room, Namespace
from sqlalchemy import and_
from flask import request
from random import choice
from copy import deepcopy
from models import db, User, Game, Field


def start_game(game):
    game.status = 'running'
    lucky = choice(game.fields)
    lucky.its_turn = True
    db.session.commit()
    for f in game.fields:
        emit('game_started', {
            'id': game.id,
            'playerTurn': f.its_turn
        }, to=f.player_id)


def get_opponent(game_id, player_id):
    field = Field.query.filter(and_(
        Field.game_id == game_id,
        Field.player_id != player_id
    )).first()
    return field.player if field else None


def emit_to_players(game: Game, *args, **kwargs):
    for f in game.fields:
        emit(*args, **kwargs, to=f.player_id)


def get_num_success_hits(field):
    if field is None:
        return 0
    return sum(sum(1 for t in row if t['tank'] and t['hit']) for row in field)


def get_game_score(game):
    if len(game.fields) == 2:
        return {
            get_opponent(game.id, field.player_id).id:
                get_num_success_hits(field.data)
            for field in game.fields if field
        }
    return dict()


def invert_field(field):
    if field is None:
        return
    return [[t if t['hit'] else {'tank': False, 'hit': False} for t in row]
            for row in field]


def finish_game(game, status, winner=None):
    game.status = status
    db.session.commit()
    if winner is None:
        emit('game_finished', {'id': game.id, 'winner': winner},
             to=get_opponent(game.id, current_user.id).id)
    else:
        emit_to_players(game, 'game_finished',
                        {'id': game.id, 'winner': winner})
    for field in game.fields:
        GamesList.send(field.player)


def reconnect():
    for field in current_user.fields:
        game = field.game
        if game.status != 'finished':
            game_score = get_game_score(game)
            opponent = get_opponent(game.id, current_user.id)
            opponent_field = Field.query.get([game.id, opponent.id])
            empty_field = [[{'tank': False, 'hit': False}] * 8] * 8
            data = {
                'id': game.id,
                'status': game.status.upper(),
                'numTanks': game.num_tanks,
                'playerTurn': field.its_turn,
                'self': {
                    'field': empty_field,
                    'score': game_score.get(current_user.id, 0),
                },
                'opponent': {
                    'username': opponent.username,
                    'field': empty_field,
                    'score': game_score.get(opponent.id, 0),
                }
            }
            if field.data is not None:
                data['self']['field'] = field.data
                if data['status'] == 'WAITING':
                    data['status'] = 'FIELD_WAS_SET'
            if opponent_field.data is not None:
                data['opponent']['field'] = invert_field(opponent_field.data)
            emit('reconnection', data, to=current_user.id)


class GamesList:

    @staticmethod
    def send(user: User):
        emit('send_list', {'gamesList': [{
            'id': game.id,
            'numTanks': game.num_tanks,
            'username': game.fields[0].player.username
        } for game in Game.query.filter_by(status='created').all()
            if game.id not in [f.game_id for f in current_user.fields]
        ]}, to=user.id)

    @staticmethod
    def add(game: Game):
        emit('add_to_list', {
            'id': game.id,
            'numTanks': game.num_tanks,
            'username': game.fields[0].player.username
        }, broadcast=True, skip_sid=request.sid)   # type: ignore

    @staticmethod
    def delete(game: Game):
        emit('del_from_list', {'id': game.id}, broadcast=True)


class GameHandler(Namespace):

    @login_required
    def on_connect(self):
        join_room(current_user.id)
        send({'message': f'User {current_user.username} connected'},
             to=current_user.id)
        reconnect()
        GamesList.send(current_user)

    @login_required
    def on_get_games_list(self):
        GamesList.send(current_user)

    @login_required
    def on_disconnect(self):
        for field in current_user.fields:
            if field.game.status != 'finished':
                if field.game.status == 'created':
                    db.session.delete(field)
                    db.session.delete(field.game)
                    db.session.commit()
                    GamesList.delete(field.game)
                else:
                    opponent = get_opponent(field.game.id, current_user.id)
                    emit('opponent_disconnected', {'id': field.game.id},
                         to=opponent.id)

    @login_required
    def on_create_game(self, data):
        self.on_disconnect()
        game = Game(num_tanks=data['numTanks'], status='created')
        db.session.add(game)
        db.session.commit()
        field = Field(game_id=game.id, player_id=current_user.id)
        db.session.add(field)
        db.session.commit()
        emit('game_created',
             {'id': game.id, 'numTanks': game.num_tanks},
             to=current_user.id)

        GamesList.add(game)

    @login_required
    def on_join(self, data):
        self.on_disconnect()
        game = Game.query.get(data['id'])
        if game.fields and len(game.fields) != 1:
            return send({'message': 'Join failed.'}, to=current_user.id)

        field = Field(game_id=game.id, player_id=current_user.id)
        db.session.add(field)
        game.status = 'waiting'
        db.session.commit()
        opponent = get_opponent(game.id, current_user.id)
        empty_field = [[{'tank': False, 'hit': False}] * 8] * 8
        data = {
            'id': game.id,
            'numTanks': game.num_tanks,
            'self': {
                'field': empty_field,
                'score': 0,
            },
            'opponent': {
                'username': opponent.username,
                'field': empty_field,
                'score': 0,
            },
        }
        opp_data = {
            'username': current_user.username,
            'field': empty_field,
            'score': 0,
        }
        emit('joined', data, to=current_user.id)
        data['opponent'] = opp_data
        emit('joined', data, to=opponent.id)
        GamesList.delete(game)

    @login_required
    def on_set_field(self, data):
        field = Field.query.get([data['id'], current_user.id])
        field.data = data['field']
        db.session.commit()

        send({'message': 'Opponent set field.'},
             to=get_opponent(field.game.id, current_user.id).id)

        game = field.game
        if len(game.fields) == 2 and all(f.data for f in game.fields):
            start_game(game)

    @login_required
    def on_move(self, data):
        game_id = data['id']
        player_field = Field.query.get([game_id, current_user.id])
        if not player_field.its_turn:
            return

        row = data['move']['row']
        col = data['move']['col']
        opponent = get_opponent(game_id, current_user.id)
        opponent_field = Field.query.get([game_id, opponent.id])
        opponent_field_data = deepcopy(opponent_field.data)
        if opponent_field_data[row][col]['hit'] is True:
            return

        opponent_field_data[row][col]['hit'] = True
        opponent_field.data = opponent_field_data

        game = player_field.game
        if get_num_success_hits(opponent_field_data) == game.num_tanks:
            finish_game(game, 'finished', current_user.username)

        if not opponent_field_data[row][col]['tank']:
            opponent_field.its_turn, player_field.its_turn = True, False

        db.session.commit()
        game_score = get_game_score(game)

        emit('move_result', {
            'id': game_id,
            'opponent': {'field': invert_field(opponent_field_data)},
            'self': {'score': game_score[current_user.id]},
            'playerTurn': player_field.its_turn,
        }, to=current_user.id)

        emit('move_result', {
            'id': game_id,
            'self': {'field': opponent_field_data},
            'opponent': {'score': game_score[opponent.id]},
            'playerTurn': opponent_field.its_turn,
        }, to=opponent.id)

    @login_required
    def on_interrupt_game(self, data):
        game = Game.query.get(data['id'])
        finish_game(game, 'finished')
