from flask_restful import Resource
from flask_login import login_user, logout_user, login_required, current_user
from pydantic import BaseModel
from werkzeug.security import check_password_hash

from models import User
from flask_pydantic import validate


class AuthRequest(BaseModel):
    username: str
    password: str
    remember: bool


class AuthResponse(BaseModel):
    username: str = ''
    message: str = ''


class AuthorizationHandler(Resource):

    @login_required
    def get(self):
        return AuthResponse(
            username=current_user.username
        ).dict(), 200

    @validate()
    def post(self, body: AuthRequest):
        user = User.query.filter_by(username=body.username).first()

        if not user or not check_password_hash(user.password, body.password):
            return AuthResponse(
                message='Please check your login details and try again.'
            ).dict(), 400

        login_user(user, remember=body.remember)

        return AuthResponse(
            username=user.username,
            message='The User logged in successfully.',
        ).dict(), 200

    @login_required
    def delete(self):
        logout_user()
        return AuthResponse(
            message='The user has successfully logged out.'
        ).dict(), 200
