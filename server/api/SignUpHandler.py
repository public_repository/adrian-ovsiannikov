from flask_pydantic import validate
from flask_restful import Resource, reqparse
from pydantic import BaseModel
from werkzeug.security import generate_password_hash

from models import db, User


parser = reqparse.RequestParser()
parser.add_argument("username", type=str)
parser.add_argument("password", type=str)


class SignUpRequest(BaseModel):
    username: str = ''
    password: str = ''


class SignUpResponse(BaseModel):
    message: str = ''


class SignUpHandler(Resource):

    @validate()
    def post(self, body: SignUpRequest):
        user = User.query.filter_by(username=body.username).first()

        if user:
            return SignUpResponse(
                message='Username address already exists.'
            ).dict(), 400

        new_user = User(
            username=body.username,
            password=generate_password_hash(body.password, method='sha256')
        )
        db.session.add(new_user)
        db.session.commit()

        return SignUpResponse(
            message='The user has been successfully registered.'
        ).dict(), 200
