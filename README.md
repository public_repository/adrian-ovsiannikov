
# Trainee App

Simple tanks web-game for trainee.

The code is based on https://gitlab.com/wemyit.trainee/adrian-ovsiannikov.

## Tutorial

## Environment variables for Docker
```bash
DB_NAME
DB_USER
DB_PASSWORD
```

## 1. Setting Up The Project

1. Clone the repository
```bash
git clone [TODO INSERT URL]
cd traineeapp
```

2. Install requirements.txt
```bash
cd server
pip install -r requirements.txt
```

3. Import the project folder into VS Code
```bash
code .
```

## 2. Running The Code Locally

1. Build the react.js front-end.
```bash
cd client
npm install
npm run build
```
2. Create the PostgreSQL tables
```bash
python main.py create_db
```
3. Start the Flask server
```bash
python main.py
```
4. Check ```localhost:3000``` in your browser to view the web application.


