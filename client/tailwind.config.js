module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {

    },
  },
  plugins: [
    // eslint-disable-next-line global-require
    require('tailwindcss'),
    // eslint-disable-next-line global-require
    require('autoprefixer'),
  ],
};
