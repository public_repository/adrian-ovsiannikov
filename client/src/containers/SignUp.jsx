import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import SignUpComponent from '../components/SignUpComponent';
import { fetchSignUp } from '../actionsCreators/authActions';

class SignUp extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      confirm: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    const { username, password, confirm } = this.state;
    if (password !== confirm) {
      return;
    }
    const { signUp } = this.props;
    signUp(username, password);
    this.setState({
      username: '',
      password: '',
      confirm: '',
    });
  }

  handleChange(event) {
    event.persist();
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  render() {
    return (
      <SignUpComponent
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
        state={this.state}
      />
    );
  }
}

SignUp.propTypes = {
  signUp: PropTypes.func,
  authorization: PropTypes.bool,
}.isRequired;

const mapDispatchToProps = (dispatch) => ({
  signUp: (un, ps) => dispatch(fetchSignUp(un, ps)),
});

const mapStateToProps = (state) => ({
  authorization: state.auth.authorization,
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
