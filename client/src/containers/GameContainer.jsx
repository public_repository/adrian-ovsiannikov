import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import {
  clearGameState,
  gameStarted,
  moveResult,
  setField,
  setGameStatus,
} from '../actionsCreators/gameActions';
import { showNotification } from '../actionsCreators/appActions';
import socket from '../socketIO';
import {
  FIELD_WAS_SET, gameStatuses, RUNNING, WAITING,
} from '../types/statusTypes';
import { fetchHistory } from '../actionsCreators/historyActions';
import FieldContainer from './Field';
import { Col, Container, Row } from '../components/Container';
import Header from '../components/Header';

function setFieldButton(isAvailable, onClick) {
  return <Button onClick={onClick} disabled={!isAvailable}>Set field</Button>;
}

class GameContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentNumTanks: 0,
      isNumTanksEnough: false,
    };
    this.handleExit = this.handleExit.bind(this);
    this.handleChangeNumTanks = this.handleChangeNumTanks.bind(this);
  }

  componentDidMount() {
    this.setSocketListeners(this.props);
  }

  handleChangeNumTanks(numTanks) {
    const { props } = this;
    this.setState({ currentNumTanks: numTanks });
    this.setState({ isNumTanksEnough: numTanks === props.numTanks });
  }

  handleExit() {
    const { id, history, clearState } = this.props;
    socket.emit('interrupt_game', { id });
    clearState();
    history.push('/');
  }

  handleSetField = () => {
    const { id, self, setStatus } = this.props;
    socket.emit('set_field', {
      id,
      field: self.field,
    });
    setStatus(FIELD_WAS_SET);
  }

  setSocketListeners = (props) => {
    socket.on('message', (payload) => {
      console.log(payload.message);
    });

    socket.on('game_started', (payload) => {
      if (props.id === payload.id) {
        props.started(payload);
      }
    });

    socket.on('move_result', (payload) => {
      if (props.id === payload.id) {
        props.moveResult(payload);
      }
    });

    socket.on('opponent_disconnected', (payload) => {
      if (props.id === payload.id) {
        props.opponentDisconnected(payload);
      }
    });

    socket.on('game_finished', (payload) => {
      if (props.id === payload.id) {
        if (!payload.winner) {
          props.showNotification(
            'Draw',
            'The opponent has forcibly ended the game.',
          );
        } else {
          props.showNotification(
            payload.winner === props.self.username
              ? 'Victory!'
              : 'Defeat!',
            `The game ended with the victory of player ${payload.winner}`,
          );
        }
        props.clearState();
        props.resetHistory();
        props.history.push('/');
      }
    });
  }

  render() {
    const {
      status, self, opponent, playerTurn, numTanks,
    } = this.props;
    const { currentNumTanks, isNumTanksEnough } = this.state;
    return (
      <Container>
        <Row>
          <Header>Game</Header>
        </Row>
        <Row>
          <Col>
            {status === RUNNING && (
            <>
              <Row>
                My score:
                {self.score}
              </Row>
              <Row>
                Opponent score:
                {opponent.score}
              </Row>
            </>
            )}
            <Row>
              {(status === RUNNING) && playerTurn ? 'Now it\'s your turn.' : 'Now it\'s the opponent\'s turn.'}
            </Row>
            <Row>
              {status === WAITING && (
                `Place ${numTanks.toString()} tanks in your half of the field. `
                + `Now ${currentNumTanks.toString()} tanks are on display.`
              )}
              {status === WAITING
                ? setFieldButton(isNumTanksEnough, this.handleSetField)
                : 'Expect an opponent.'}
            </Row>
            <Row><Button onClick={this.handleExit}>Exit</Button></Row>
          </Col>
          <Col>
            <FieldContainer
              field={[].concat(self.field)}
              itsMy
              handleChangeNumTanks={this.handleChangeNumTanks}
              isNumTanksEnough={isNumTanksEnough}
            />
          </Col>
          <Col>
            <FieldContainer
              field={[].concat(opponent.field)}
              itsMy={false}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

GameContainer.propTypes = {
  authorization: PropTypes.bool,
  playerTurn: PropTypes.bool,
  status: PropTypes.oneOf(gameStatuses),
  numTanks: PropTypes.number,
  self: PropTypes.shape({
    score: PropTypes.number,
    field: PropTypes.arrayOf(PropTypes.array),
  }),
  opponent: PropTypes.shape({
    score: PropTypes.number,
    field: PropTypes.arrayOf(PropTypes.array),
  }),
}.isRequired;

const mapStateToProps = (state) => ({
  ...state.game,
  authorization: state.auth.authorization,
  self: {
    ...state.game.self,
    username: state.auth.username,
  },
});

const mapDispatchToProps = (dispatch) => ({
  started: (payload) => dispatch(gameStarted(payload)),
  moveResult: (payload) => dispatch(moveResult(payload)),
  opponentDisconnected: () => dispatch(showNotification(
    'Loss of connection with an opponent',
    'The connection with the opponent is lost.'
        + ' You can wait for your opponent to reconnect or start  new game.',
  )),
  clearState: () => dispatch(clearGameState()),
  showNotification: (header, message) => dispatch(showNotification(header, message)),
  setStatus: (status) => dispatch(setGameStatus(status)),
  resetHistory: () => dispatch(fetchHistory()),
  setField: (field) => dispatch(setField(field)),
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(GameContainer));
