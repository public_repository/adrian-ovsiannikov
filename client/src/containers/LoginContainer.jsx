import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import LoginComponent from '../components/LoginComponent';
import { fetchLogin } from '../actionsCreators/authActions';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      remember: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    const { username, password, remember } = this.state;
    const { logIn } = this.props;

    logIn(username, password, remember);
    this.setState({
      username: '',
      password: '',
      remember: false,
    });
  }

  handleChange(event) {
    event.persist();
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  render() {
    return (
      <LoginComponent
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
        state={this.state}
        props={this.props}
      />
    );
  }
}

Login.propTypes = {
  logIn: PropTypes.func,
}.isRequired;

const mapDispatchToProps = (dispatch) => ({
  logIn: (un, ps, rm) => dispatch(fetchLogin(un, ps, rm)),
});

const mapStateToProps = (state) => ({
  authorization: state.auth.authorization,
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
