import React, { Component } from 'react';
import { Button, Form } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { gameCreated, reconnectionToGame, toGame } from '../actionsCreators/gameActions';
import socket from '../socketIO';
import { CREATED, gameStatuses } from '../types/statusTypes';
import Header from '../components/Header';

class CreateGameContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      range: 0,
      currentNumTanks: 20,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.setSocketListeners(this.props);
  }

  handleSubmit(event) {
    event.preventDefault();
    const { currentNumTanks } = this.state;
    socket.emit('create_game', {
      numTanks: currentNumTanks,
    });
  }

  handleChange(event) {
    event.preventDefault();
    const { name, value } = event.target;
    this.setState({ [name]: value });
    this.setState({ currentNumTanks: Math.round(((value / 100) * 24) + 8) });
  }

  setSocketListeners = (props) => {
    socket.on('message', (payload) => {
      console.log(payload.message);
    });

    socket.on('game_created', (payload) => {
      props.created(payload);
    });

    socket.on('joined', (payload) => {
      const { joined, history } = props;
      joined(payload);
      history.push(`/game/${payload.id}`);
    });

    socket.on('reconnection', (payload) => {
      const { reconnection, history } = props;
      reconnection(payload);
      history.push(`/game/${payload.id}`);
    });
  }

  render() {
    const { currentNumTanks, range } = this.state;
    const { numTanks, status } = this.props;
    return (
      <>
        <Header>Create game</Header>
        <Form onChange={this.handleChange} onSubmit={this.handleSubmit}>
          <Form.Group controlId="formBasicRange">
            <Form.Label>
              You can set the number of tanks.
              The current value is
              {' '}
              {currentNumTanks}
              .
            </Form.Label>
            <Form.Row>
              <Form.Control type="range" />
            </Form.Row>
            <Form.Row>
              <Button
                name="range"
                value={range}
                variant="primary"
                type="submit"
              >
                Create
              </Button>
            </Form.Row>
          </Form.Group>
          {status === CREATED
            && (
            <Form.Group controlId="">
              <Form.Label>
                The game for
                {' '}
                {numTanks}
                {' '}
                tanks has been successfully created.
                Expect the opponent to join.
              </Form.Label>
            </Form.Group>
            )}
        </Form>
      </>
    );
  }
}

CreateGameContainer.propTypes = {
  numTanks: PropTypes.number,
  status: PropTypes.oneOf(gameStatuses),
}.isRequired;

const mapDispatchToProps = (dispatch) => ({
  created: (payload) => dispatch(gameCreated(payload)),
  joined: (payload) => dispatch(toGame(payload)),
  reconnection: (payload) => dispatch(reconnectionToGame(payload)),
});
const matStateToProps = (state) => state.game;

export default connect(matStateToProps, mapDispatchToProps)(withRouter(CreateGameContainer));
