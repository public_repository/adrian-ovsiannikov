import React from 'react';
import { Button, Modal } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { hideNotification } from '../actionsCreators/appActions';

function Notification({ notification, close }) {
  return (
    <>
      <Modal show={!((!notification.header) && (!notification.message))} onHide={close} centered>
        <Modal.Header closeButton>
          <Modal.Title>{notification.header}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {notification.message}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={close}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

Notification.propTypes = {
  close: PropTypes.func,
  notification: PropTypes.shape({
    header: PropTypes.string,
    message: PropTypes.string,
  }),
}.isRequired;

const mapDispatchToProps = (dispatch) => ({
  close: () => dispatch(hideNotification()),
});

const mapStateToProps = (state) => ({
  notification: state.app.notification,
});

export default connect(mapStateToProps, mapDispatchToProps)(Notification);
