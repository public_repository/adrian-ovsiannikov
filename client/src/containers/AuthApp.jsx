import React from 'react';
import { Route } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Navigation from '../components/Navigation';
import History from '../components/History';
import GameMenu from '../components/GameMenu';
import Game from './GameContainer';
import { Container } from '../components/Container';

const AuthAppContainer = () => {
  const { id } = useSelector((state) => state.game);
  return (
    <>
      <Navigation />
      <Container>
        <Route exact path="/" component={GameMenu} />
        <Route exact path={`/game/${id}`} component={Game} />
        <Route exact path="/history" component={History} />
      </Container>
    </>
  );
};

export default AuthAppContainer;
