import React, { Component } from 'react';
import { connect } from 'react-redux';
import socket from '../socketIO';
import { addToGamesList, delFromGamesList, setGamesList } from '../actionsCreators/gamesListActions';
import GamesListComponent from '../components/GamesListComponent';
import { joinGame } from '../actionsCreators/gameActions';
import { Container } from '../components/Container';
import Header from '../components/Header';

class GamesListContainer extends Component {
  constructor(props) {
    super(props);
    this.handleJoin = this.handleJoin.bind(this);
  }

  componentDidMount() {
    this.setSocketListeners(this.props);
  }

  handleJoin = (id) => {
    joinGame(id);
    socket.emit('join', { id });
  }

  setSocketListeners = (props) => {
    socket.on('send_list', (payload) => {
      props.setList(payload);
    });

    socket.on('add_to_list', (payload) => {
      props.addToList(payload);
    });

    socket.on('del_from_list', (payload) => {
      props.delFromList(payload);
    });
  }

  render() {
    return (
      <Container>
        <Header>List of games available to join:</Header>
        <GamesListComponent join={this.handleJoin} />
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  ...state.game,
  authorization: state.auth.authorization,
  self: {
    ...state.game.self,
    username: state.auth.username,
  },
});

const mapDispatchToProps = (dispatch) => ({
  setList: (payload) => dispatch(setGamesList(payload)),
  addToList: (payload) => dispatch(addToGamesList(payload)),
  delFromList: (payload) => dispatch(delFromGamesList(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(GamesListContainer);
