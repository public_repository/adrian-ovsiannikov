import { ButtonGroup } from 'react-bootstrap';
import { connect } from 'react-redux';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { RUNNING, WAITING } from '../types/statusTypes';
import socket from '../socketIO';
import { onMove, setField } from '../actionsCreators/gameActions';
import FieldButtonComponent from '../components/FieldButtonComponent';

const getNumTanks = (field) => {
  let num = 0;
  field.map((row) => {
    num += row.filter((target) => target.tank).length;
    return num;
  });
  return num;
};

class FieldContainer extends Component {
  constructor(props) {
    super(props);
    this.handleMove = this.handleMove.bind(this);
  }

  handleMove = ({ row, col }) => {
    const { props } = this;
    const newField = [].concat(props.field);
    const target = props.field[row][col];

    if (props.game.status === WAITING) {
      newField[row][col] = target.tank
        ? { tank: false, hit: false }
        : { tank: true, hit: false };
      props.setField(newField);
      props.handleChangeNumTanks(getNumTanks(newField));
    } else if (props.game.status === RUNNING) {
      props.onMove();
      target.hit = true;
      newField[row][col] = target;
      socket.emit('move', {
        id: props.game.id,
        move: { row, col },
      });
    }
  }

  render() {
    const {
      field, itsMy, game, isNumTanksEnough,
    } = this.props;
    return (
      <>
        <ButtonGroup vertical>
          {field.map((row, rowIndex) => (
            <ButtonGroup key={String(rowIndex)}>
              {row.map((target, colIndex) => (
                <FieldButtonComponent
                  key={String(colIndex)}
                  target={target}
                  row={rowIndex}
                  col={colIndex}
                  itsMy={itsMy}
                  handleMove={this.handleMove}
                  game={game}
                  isNumTanksEnough={isNumTanksEnough}
                />
              ))}
            </ButtonGroup>
          ))}
        </ButtonGroup>
      </>
    );
  }
}

FieldContainer.propTypes = {
  game: PropTypes.object,
  isNumTanksEnough: PropTypes.bool,
  itsMy: PropTypes.bool,
  field: PropTypes.arrayOf(
    PropTypes.arrayOf(
      PropTypes.shape({
        tank: PropTypes.bool.isRequired,
        hit: PropTypes.bool.isRequired,
      }).isRequired,
    ).isRequired,
  ).isRequired,
}.isRequired;

const mapStateToProps = (state) => ({ game: state.game });

const mapDispatchToProps = (dispatch) => ({
  setField: (field) => dispatch(setField(field)),
  onMove: () => dispatch(onMove()),
});

export default connect(mapStateToProps, mapDispatchToProps)(FieldContainer);
