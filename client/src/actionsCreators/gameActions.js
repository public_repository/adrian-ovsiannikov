import {
  GAME_CREATED,
  JOIN_GAME,
  TO_GAME,
  MOVE_RESULT,
  SET_FIELD,
  INTERRUPT_GAME,
  SET_GAME_STATUS,
  CLEAR_GAME_STATE,
  RECONNECTION_TO_GAME,
  ON_MOVE,
  GAME_STARTED,
} from '../types/actionsTypes';

export function setGameStatus(status) {
  return {
    type: SET_GAME_STATUS,
    status,
  };
}

export function joinGame(id) {
  return {
    type: JOIN_GAME,
    id,
  };
}

export function gameCreated(payload) {
  return {
    type: GAME_CREATED,
    payload: {
      id: payload.id,
      numTanks: payload.numTanks,
    },
  };
}

export function moveResult(payload) {
  return {
    type: MOVE_RESULT,
    payload,
  };
}

export function toGame(payload) {
  return {
    type: TO_GAME,
    payload,
  };
}

export function setField(field) {
  return {
    type: SET_FIELD,
    field,
  };
}

export function interruptGame(id) {
  return {
    type: INTERRUPT_GAME,
    id,
  };
}

export function clearGameState() {
  return {
    type: CLEAR_GAME_STATE,
  };
}

export function reconnectionToGame(payload) {
  return {
    type: RECONNECTION_TO_GAME,
    payload,
  };
}

export function onMove() {
  return {
    type: ON_MOVE,
  };
}

export function gameStarted(payload) {
  return {
    type: GAME_STARTED,
    payload,
  };
}
