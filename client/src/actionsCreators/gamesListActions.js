import { ADD_TO_LIST, DEL_FROM_LIST, SET_LIST } from '../types/actionsTypes';

export function setGamesList(payload) {
  return {
    type: SET_LIST,
    list: payload.gamesList,
  };
}

export function addToGamesList(payload) {
  return {
    type: ADD_TO_LIST,
    game: payload,
  };
}

export function delFromGamesList(payload) {
  return {
    type: DEL_FROM_LIST,
    id: payload.id,
  };
}
