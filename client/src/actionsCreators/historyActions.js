import { CLEAR_HISTORY, FETCHED_HISTORY, HISTORY_RECEIVED } from '../types/actionsTypes';

export function fetchHistory() {
  return {
    type: FETCHED_HISTORY,
  };
}

export function historyReceived(payload) {
  return {
    type: HISTORY_RECEIVED,
    payload,
  };
}

export function clearHistory() {
  return {
    type: CLEAR_HISTORY,
  };
}
