import {
  FETCHED_LOGIN, FETCHED_LOGOUT, FETCHED_SIGN_UP, LOGIN_SUCCEEDED, LOGOUT_SUCCEEDED,
} from '../types/actionsTypes';
import socket from '../socketIO';

export function fetchLogin(username, password, remember) {
  return {
    type: FETCHED_LOGIN,
    login: {
      username,
      password,
      remember,
    },
  };
}

export function loginSucceeded(username) {
  socket.connect();
  return {
    type: LOGIN_SUCCEEDED,
    username,
  };
}

export function fetchLogout() {
  return {
    type: FETCHED_LOGOUT,
  };
}

export function logoutSucceeded() {
  socket.disconnect();
  return {
    type: LOGOUT_SUCCEEDED,
  };
}

export function fetchSignUp(username, password) {
  return {
    type: FETCHED_SIGN_UP,
    login: {
      username,
      password,
    },
  };
}
