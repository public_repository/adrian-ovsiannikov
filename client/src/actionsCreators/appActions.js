import {
  HIDE_LOADER, SHOW_LOADER, HIDE_NOTIFICATION, SHOW_NOTIFICATION, INIT_API_REQUEST,
} from '../types/actionsTypes';

export function showLoader() {
  return {
    type: SHOW_LOADER,
  };
}

export function hideLoader() {
  return {
    type: HIDE_LOADER,
  };
}

export function showNotification(header, message) {
  return {
    type: SHOW_NOTIFICATION,
    notification: {
      header,
      message,
    },
  };
}

export function hideNotification() {
  return {
    type: HIDE_NOTIFICATION,
  };
}

export function initApiRequest() {
  return {
    type: INIT_API_REQUEST,
  };
}
