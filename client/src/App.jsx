import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import AuthApp from './containers/AuthApp';
import AnonApp from './components/AnonApp';
import Loader from './components/Loader';
import Notification from './containers/Notification';

const App = ({ authorization }) => (
  <>
    {authorization
      ? <AuthApp />
      : <AnonApp />}
    <Loader />
    <Notification />
  </>
);

App.propTypes = {
  authorization: PropTypes.bool,
}.isRequired;

const mapStateToProps = (state) => state.auth;

export default connect(mapStateToProps)(App);
