import { ADD_TO_LIST, DEL_FROM_LIST, SET_LIST } from '../types/actionsTypes';

const initialState = {
  list: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_LIST:
      return { list: [...new Set(action.list)] };
    case ADD_TO_LIST:
      return { list: [...new Set([action.game].concat(state.list))] };
    case DEL_FROM_LIST:
      return { list: state.list.filter((game) => game.id !== action.id) };
    default:
      return state;
  }
};
