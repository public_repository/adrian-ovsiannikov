import { LOGIN_SUCCEEDED, LOGOUT_SUCCEEDED } from '../types/actionsTypes';

const initialState = {
  authorization: false,
  username: '',
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCEEDED:
      return {
        ...state,
        authorization: true,
        username: action.username,
      };
    case LOGOUT_SUCCEEDED:
      return initialState;
    default:
      return state;
  }
};
