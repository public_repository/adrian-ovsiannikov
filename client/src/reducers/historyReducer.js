import { CLEAR_HISTORY, HISTORY_RECEIVED } from '../types/actionsTypes';

const initialState = {
  headers: [],
  data: [[]],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case HISTORY_RECEIVED:
      return {
        ...state, ...action.payload,
      };
    case CLEAR_HISTORY:
      return initialState;
    default:
      return state;
  }
};
