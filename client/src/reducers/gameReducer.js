import {
  CLEAR_GAME_STATE,
  GAME_CREATED, ON_MOVE,
  TO_GAME,
  MOVE_RESULT, RECONNECTION_TO_GAME,
  SET_FIELD,
  SET_GAME_STATUS, GAME_STARTED,
} from '../types/actionsTypes';
import { CREATED, RUNNING, WAITING } from '../types/statusTypes';

const initialState = {
  id: null,
  status: '',
  numTanks: 0,
  playerTurn: false,
  self: {
    field: [[]],
    score: 0,
  },
  opponent: {
    username: '',
    field: [[]],
    score: 0,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_GAME_STATUS:
      return { ...state, status: action.status };
    case GAME_CREATED:
      return { ...state, status: CREATED, ...action.payload };
    case TO_GAME:
      return { ...state, ...action.payload, status: WAITING };
    case GAME_STARTED:
      return { ...state, status: RUNNING, ...action.payload };
    case ON_MOVE:
      return { ...state, playerTurn: false };
    case MOVE_RESULT:
      return {
        ...state,
        playerTurn: action.payload.playerTurn,
        self: { ...state.self, ...action.payload.self },
        opponent: { ...state.opponent, ...action.payload.opponent },
      };
    case SET_FIELD:
      return { ...state, self: { ...state.self, field: action.field } };
    case RECONNECTION_TO_GAME:
      return { ...state, ...action.payload };
    case CLEAR_GAME_STATE:
      return initialState;
    default:
      return state;
  }
};
