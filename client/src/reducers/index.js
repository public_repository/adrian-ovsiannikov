import app from './appReducer';
import auth from './authReducer';
import history from './historyReducer';
import game from './gameReducer';
import gamesList from './gamesListReducer';

export default {
  app,
  auth,
  history,
  game,
  gamesList,
};
