import {
  SHOW_NOTIFICATION,
  HIDE_NOTIFICATION,
  SHOW_LOADER,
  HIDE_LOADER,
} from '../types/actionsTypes';

const initialState = {
  loading: false,
  notification: {
    header: null,
    message: null,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SHOW_LOADER:
      return { ...state, loading: true };
    case HIDE_LOADER:
      return { ...state, loading: false };
    case SHOW_NOTIFICATION:
      return { ...state, notification: action.notification };
    case HIDE_NOTIFICATION:
      return { ...state, notification: initialState.notification };
    default:
      return state;
  }
};
