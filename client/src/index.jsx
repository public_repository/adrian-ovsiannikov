import React from 'react';
import { render } from 'react-dom';
import {
  compose, createStore, applyMiddleware, combineReducers,
} from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import thunk from 'redux-thunk';
import { ConnectedRouter, connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';

import './index.css';
import App from './App';
import { initApiRequest } from './actionsCreators/appActions';
import reducers from './reducers';
import rootSaga from './sagas/rootSaga';

const history = createBrowserHistory();
const reducer = combineReducers({ ...reducers, router: connectRouter(history) });
const router = routerMiddleware(history);
const saga = createSagaMiddleware();
const store = createStore(
  reducer,
  compose(
    applyMiddleware(thunk, saga, router),
    // eslint-disable-next-line no-underscore-dangle
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  ),
);

saga.run(rootSaga);
store.dispatch(initApiRequest());

const app = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>
);

render(app, document.getElementById('root'));
