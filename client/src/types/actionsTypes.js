// App
export const SHOW_NOTIFICATION = 'SHOW_NOTIFICATION';
export const HIDE_NOTIFICATION = 'HIDE_NOTIFICATION';
export const SHOW_LOADER = 'SHOW_LOADER';
export const HIDE_LOADER = 'HIDE_LOADER';
export const INIT_API_REQUEST = 'INIT_API_REQUEST';

// auth
export const FETCHED_LOGIN = 'FETCHED_LOGIN';
export const LOGIN_SUCCEEDED = 'LOGIN_SUCCEEDED';

export const FETCHED_LOGOUT = 'FETCHED_LOGOUT';
export const LOGOUT_SUCCEEDED = 'LOGOUT_SUCCEEDED';

// sing up
export const FETCHED_SIGN_UP = 'FETCHED_SIGN_UP';

// history
export const FETCHED_HISTORY = 'FETCHED_HISTORY';
export const HISTORY_RECEIVED = 'HISTORY_RECEIVED';
export const CLEAR_HISTORY = 'CLEAR_HISTORY';

// games list
export const SET_LIST = 'SET_LIST';
export const ADD_TO_LIST = 'ADD_TO_LIST';
export const DEL_FROM_LIST = 'DEL_FROM_LIST';

// games
export const SET_GAME_STATUS = 'SET_GAME_STATUS';
export const JOIN_GAME = 'JOIN_GAME';
export const GAME_CREATED = 'GAME_CREATED';
export const MOVE_RESULT = 'MOVE_RESULT';
export const TO_GAME = 'TO_GAME';
export const SET_FIELD = 'SET_FIELD';
export const INTERRUPT_GAME = 'INTERRUPT_GAME';
export const CLEAR_GAME_STATE = 'CLEAR_GAME_STATE';
export const RECONNECTION_TO_GAME = 'RECONNECTION_TO_GAME';
export const ON_MOVE = 'ON_MOVE';
export const GAME_STARTED = 'GAME_STARTED';
