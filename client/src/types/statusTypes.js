// game
export const CREATED = 'CREATED';
export const WAITING = 'WAITING';
export const RUNNING = 'RUNNING';
export const FINISHED = 'FINISHED';
export const FIELD_WAS_SET = 'FIELD_WAS_SET';

export const gameStatuses = [CREATED, WAITING, RUNNING, FINISHED, FIELD_WAS_SET];
