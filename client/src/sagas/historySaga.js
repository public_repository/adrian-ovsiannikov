import { call, put, takeEvery } from 'redux-saga/effects';
import { FETCHED_HISTORY } from '../types/actionsTypes';
import { showNotification } from '../actionsCreators/appActions';
import { historyReceived } from '../actionsCreators/historyActions';

async function requestHistory() {
  return fetch(`${window.location.origin}/api/history`, {
    method: 'GET',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    credentials: 'include',
  });
}

function* historySaga() {
  try {
    const response = yield call(requestHistory);
    const payload = yield response.json();
    if (response.status === 200) {
      yield put(historyReceived(payload));
    }
  } catch (error) {
    yield put(showNotification('Error', error.message));
  }
}

export default function* watchHistoryFetch() {
  yield takeEvery(FETCHED_HISTORY, historySaga);
}
