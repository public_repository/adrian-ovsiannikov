import { call, put, takeEvery } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import { loginSucceeded, logoutSucceeded } from '../actionsCreators/authActions';
import { FETCHED_LOGIN, FETCHED_LOGOUT } from '../types/actionsTypes';
import { hideLoader, showLoader, showNotification } from '../actionsCreators/appActions';
import { clearHistory, fetchHistory } from '../actionsCreators/historyActions';

async function requestLogin(login) {
  return fetch(`${window.location.origin}/api/authorization`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    credentials: 'include',
    body: JSON.stringify(login),
  });
}

function* loginSaga(action) {
  yield put(showLoader());
  try {
    const response = yield call(requestLogin, action.login);
    if (response.status === 200) {
      yield put(loginSucceeded(action.login.username));
      yield put(fetchHistory());
      yield put(push('/'));
    } else {
      const json = yield response.json();
      yield put(showNotification('Failed', json.message));
    }
  } catch (error) {
    yield put(showNotification('Error', error.message));
  }
  yield put(hideLoader());
}

async function requestLogout() {
  return fetch(`${window.location.origin}/api/authorization`, {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    credentials: 'include',
  });
}

function* logoutSaga() {
  yield put(showLoader());
  try {
    const response = yield call(requestLogout);
    if (response.status === 200) {
      yield put(logoutSucceeded());
      yield put(clearHistory());
      yield put(push('/'));
    } else {
      const json = yield response.json();
      yield put(showNotification('Failed', json.message));
    }
  } catch (error) {
    yield put(showNotification('Error', error.message));
  }
  yield put(hideLoader());
}

export default function* watchAuthFetch() {
  yield takeEvery(FETCHED_LOGIN, loginSaga);
  yield takeEvery(FETCHED_LOGOUT, logoutSaga);
}
