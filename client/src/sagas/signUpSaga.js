import { call, put, takeEvery } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import { FETCHED_SIGN_UP } from '../types/actionsTypes';
import {
  hideLoader, showLoader, showNotification,
} from '../actionsCreators/appActions';

async function requestSignUp(login) {
  return fetch(`${window.location.origin}/api/sign-up`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    credentials: 'same-origin',
    body: JSON.stringify(login),
  });
}

function* signUpSaga(action) {
  yield put(showLoader());
  try {
    const response = yield call(requestSignUp, action.login);
    const json = yield response.json();
    if (response.status === 200) {
      yield put(showNotification('Success', json.message));
      yield put(push('/login'));
    } else {
      yield put(showNotification('Failed', json.message));
    }
  } catch (error) {
    yield put(showNotification('Error', error.message));
  }
  yield put(hideLoader());
}

export default function* watchSignUpFetch() {
  yield takeEvery(FETCHED_SIGN_UP, signUpSaga);
}
