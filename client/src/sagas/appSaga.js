import { call, put, takeEvery } from 'redux-saga/effects';
import { loginSucceeded } from '../actionsCreators/authActions';
import { INIT_API_REQUEST } from '../types/actionsTypes';
import { hideLoader, showLoader, showNotification } from '../actionsCreators/appActions';
import { fetchHistory } from '../actionsCreators/historyActions';

async function requestInit() {
  return fetch(`${window.location.origin}/api/authorization`, {
    method: 'GET',
    headers: { 'Content-Type': 'application/json;charset=utf-8' },
    credentials: 'include',
  });
}

function* initSaga() {
  yield put(showLoader());
  try {
    const response = yield call(requestInit);
    if (response.status === 200) {
      const json = yield response.json();
      yield put(loginSucceeded(json.username));
      yield put(fetchHistory());
    }
  } catch (error) {
    yield put(showNotification('Error', error.message));
  }
  yield put(hideLoader());
}

export default function* watchAppFetch() {
  yield takeEvery(INIT_API_REQUEST, initSaga);
}
