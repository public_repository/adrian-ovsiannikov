import { all } from 'redux-saga/effects';
import watchAuthFetch from './authSaga';
import watchSignUpFetch from './signUpSaga';
import watchHistoryFetch from './historySaga';
import watchAppFetch from './appSaga';

export default function* rootSaga() {
  yield all([
    watchAuthFetch(),
    watchSignUpFetch(),
    watchHistoryFetch(),
    watchAppFetch(),
  ]);
}
