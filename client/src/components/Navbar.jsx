import { Link } from 'react-router-dom';
import React from 'react';
// eslint-disable-next-line react/prop-types
export const Navbar = ({ children }) => <nav className="flex relative items-center py-2 px-4 flex-row flex-nowrap justify-start bg-gray-50">{children}</nav>;
// eslint-disable-next-line react/prop-types
export const NavbarBrand = ({ children, href }) => <a href={href} className="inline-block py-1 mr-4 text-xl text-black whitespace-nowrap">{children}</a>;
// eslint-disable-next-line react/prop-types
export const NavbarNav = ({ children, className }) => <div className={`flex flex-row pl-0 mb-0 list-none list-outside ${className}`}>{children}</div>;
// eslint-disable-next-line react/prop-types
export const NavLink = ({ children, to }) => <Link className="block p-2 bg-gray-50" to={to}>{children}</Link>;
// eslint-disable-next-line react/prop-types,react/button-has-type
export const NavButton = ({ children, onClick }) => <button type="button" className="block p-2 bg-gray-50 border-none" onClick={onClick}>{children}</button>;
