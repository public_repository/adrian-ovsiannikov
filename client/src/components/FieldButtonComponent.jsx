import { Button } from 'react-bootstrap';
import React from 'react';
import { FIELD_WAS_SET, RUNNING, WAITING } from '../types/statusTypes';

const getValue = ({ tank, hit }) => {
  if (tank) { return hit ? 'X' : 'T'; }
  return hit ? '*' : '_';
};

function FieldButtonComponent({
  target, row, col, game, itsMy, handleMove, isNumTanksEnough,
}) {
  const value = getValue(target);
  const move = { row, col };

  if (game.status === RUNNING) {
    if (!itsMy && !game.playerTurn) {
      return <Button disabled variant="secondary">{value}</Button>;
    } if (itsMy && !game.playerTurn) {
      return <Button disabled variant="primary">{value}</Button>;
    } if (itsMy && game.playerTurn) {
      return <Button disabled variant="secondary">{value}</Button>;
    } if (!itsMy && game.playerTurn) {
      return target.hit
        ? <Button disabled variant="primary">{value}</Button>
        : <Button onClick={() => handleMove(move)} variant="primary">{value}</Button>;
    }
  } else if (game.status === WAITING) {
    if (itsMy) {
      if (isNumTanksEnough) {
        return target.tank
          ? <Button onClick={() => handleMove(move)} variant="primary">{value}</Button>
          : <Button disabled variant="primary">{value}</Button>;
      }
      return <Button onClick={() => handleMove(move)} variant="primary">{value}</Button>;
    }
    return <Button disabled variant="secondary">{value}</Button>;
  } else if (game.status === FIELD_WAS_SET) {
    return <Button disabled variant="secondary">{value}</Button>;
  }
}

export default FieldButtonComponent;

// function SelfFieldButton({
//   target, row, col, handleMove, isNumTanksEnough,
// }) {
//   const { status, playerTurn } = useSelector((state) => state.game);
//   const value = getValue(target);
//   const move = { row, col };
//   if (status === RUNNING) {
//     return <Button disabled variant={playerTurn ? 'secondary' : 'primary'}>{value}</Button>;
//   } if (status === WAITING) {
//     if (isNumTanksEnough) {
//       return <Button disabled={!target.tank} onClick={() => handleMove(move)}
//       variant="primary">{value}</Button>;
//     }
//     return <Button onClick={() => handleMove(move)} variant="primary">{value}</Button>;
//   } if (status === FIELD_WAS_SET) {
//     return <Button disabled variant="secondary">{value}</Button>;
//   }
// }
// const OpponentFieldButton = () => {};
