import { Jumbotron, ListGroup } from 'react-bootstrap';
import { connect, useDispatch } from 'react-redux';
import React from 'react';
import PropTypes from 'prop-types';
import { joinGame } from '../actionsCreators/gameActions';
import socket from '../socketIO';

const GamesRow = ({ game }) => {
  const { id, username, numTanks } = game;
  const dispatch = useDispatch();
  const join = () => {
    socket.emit('join', { id });
    dispatch(joinGame(id));
  };
  return (
    <ListGroup.Item action onClick={join}>
      {`${username}, ${numTanks} tanks`}
    </ListGroup.Item>
  );
};

GamesRow.propTypes = {
  id: PropTypes.number.isRequired,
  username: PropTypes.string.isRequired,
  numTanks: PropTypes.number.isRequired,
}.isRequired;

const GameListComponent = ({ list }) => (
  <Jumbotron>
    <ListGroup>
      {list.map((game) => <GamesRow key={game.id} game={game} />)}
    </ListGroup>
  </Jumbotron>
);

GameListComponent.propTypes = {
  list: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
  })).isRequired,
}.isRequired;

const mapStateToProps = (state) => state.gamesList;

export default connect(mapStateToProps)(GameListComponent);
