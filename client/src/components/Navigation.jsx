import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { fetchLogout } from '../actionsCreators/authActions';
import {
  Navbar, NavbarBrand, NavbarNav, NavButton, NavLink,
} from './Navbar';
import { CREATED } from '../types/statusTypes';

const Navigation = ({ username, game, dispatch }) => (
  <Navbar>
    <NavbarBrand href="/">TraineeApp</NavbarBrand>
    <NavbarNav className="mr-auto">
      <NavLink to={(!game.status || game.status === CREATED) ? '/' : `/game/${game.id}`}>Game</NavLink>
      <NavLink to="/history">History</NavLink>
    </NavbarNav>
    <NavbarNav>
      <NavLink to="/history">{username}</NavLink>
      <NavButton onClick={() => dispatch(fetchLogout())}>Logout</NavButton>
    </NavbarNav>
  </Navbar>
);

Navigation.propTypes = {
  username: PropTypes.string,
}.isRequired;

const mapStateToProps = (state) => ({
  username: state.auth.username,
  game: state.game,
});

export default connect(mapStateToProps)(Navigation);
