import React from 'react';

export default ({ children }) => <h4 className="text-current text-xl text-center p-6">{children}</h4>;
