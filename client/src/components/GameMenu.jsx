import React from 'react';
import { Container, Row, Col } from './Container';
import GamesListContainer from '../containers/GamesListContainer';
import CreateGameContainer from '../containers/CreateGameContainer';

const GameMenuComponent = () => (
  <Container>
    <Row>
      <Col><GamesListContainer /></Col>
      <Col><CreateGameContainer /></Col>
    </Row>
  </Container>
);

export default GameMenuComponent;
