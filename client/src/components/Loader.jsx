import React from 'react';
import { connect } from 'react-redux';
import { Spinner } from 'react-bootstrap';
import PropTypes from 'prop-types';

function Loader({ loading }) {
  let animation = '';
  if (loading) {
    animation = 'border';
  }
  return (
    <Spinner animation={animation} role="status">
      <span className="sr-only">Loading...</span>
    </Spinner>
  );
}

Loader.propTypes = {
  loading: PropTypes.bool,
}.isRequired;

const mapStateToProps = (state) => state.app;

export default connect(mapStateToProps)(Loader);
