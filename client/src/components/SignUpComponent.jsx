import { Button, Form } from 'react-bootstrap';
import React from 'react';
import PropTypes from 'prop-types';
import { Container } from './Container';

function SignUpComponent({ handleChange, handleSubmit, state }) {
  return (
    <Container>
      <h2>
        Sign Up
      </h2>
      <Form onChange={handleChange} onSubmit={handleSubmit}>
        <Form.Group controlId="formUsername">
          <Form.Control
            placeholder="Username"
            name="username"
            value={state.username}
          />
        </Form.Group>
        <Form.Group controlId="formPassword">
          <Form.Control
            type="password"
            placeholder="Password"
            name="password"
            value={state.password}
          />
        </Form.Group>
        <Form.Group controlId="formPassword">
          <Form.Control
            type="password"
            placeholder="Confirm the password"
            name="confirm"
            value={state.confirm}
          />
        </Form.Group>
        <Form.Row>
          <Button variant="primary" type="submit">
            Sing up
          </Button>
        </Form.Row>
      </Form>
    </Container>
  );
}

SignUpComponent.propTypes = {
  handleChange: PropTypes.func,
  handleSubmit: PropTypes.func,
  state: PropTypes.shape({
    username: PropTypes.string,
    password: PropTypes.string,
    confirm: PropTypes.string,
  }),
}.isRequired;

export default SignUpComponent;
