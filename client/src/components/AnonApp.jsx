import { Route } from 'react-router-dom';
import React from 'react';
import Login from '../containers/LoginContainer';
import SignUp from '../containers/SignUp';
import {
  Navbar, NavbarBrand, NavbarNav, NavLink,
} from './Navbar';
import Header from './Header';

const Home = () => (
  <Header>To access the game, you need to log in.</Header>
);

const AnonNavigation = () => (
  <Navbar>
    <NavbarBrand href="/">TraineeApp</NavbarBrand>
    <NavbarNav className="mr-auto">
      <NavLink to="/">Home</NavLink>
    </NavbarNav>
    <NavbarNav>
      <NavLink to="/login">Login</NavLink>
      <NavLink to="/sign-up">Sing Up</NavLink>
    </NavbarNav>
  </Navbar>
);

const AnonApp = () => (
  <>
    <AnonNavigation />
    <Route exact path="/" component={Home} />
    <Route exact path="/login" component={Login} />
    <Route exact path="/sign-up" component={SignUp} />
  </>
);

export default AnonApp;
