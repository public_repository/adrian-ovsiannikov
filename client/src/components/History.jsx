import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Table, TableData, TableHeader } from './Table';
import { Container } from './Container';
import Header from './Header';

const TableComponent = ({ headers, data }) => (
  <Container>
    <Header>
      Match history table
    </Header>
    <Table>
      <thead>
        <tr>
          {headers.map((header) => (
            <TableHeader>{header}</TableHeader>
          ))}
        </tr>
      </thead>
      <tbody>
        {data.map((row) => (
          <tr>
            {row.map((value) => (
              <TableData>{value}</TableData>
            ))}
          </tr>
        ))}
      </tbody>
    </Table>
  </Container>
);

TableComponent.propTypes = {
  headers: PropTypes.arrayOf(PropTypes.string),
  data: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
}.isRequired;

const mapStateToProps = (state) => state.history;

const History = connect(mapStateToProps)(TableComponent);
export default History;
