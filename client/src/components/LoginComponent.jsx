import { Button, Form } from 'react-bootstrap';
import React from 'react';
import PropTypes from 'prop-types';
import { Container } from './Container';

function LoginComponent({ handleChange, handleSubmit, state }) {
  return (
    <Container>
      <h2>
        Login
      </h2>
      <Form onChange={handleChange} onSubmit={handleSubmit}>
        <Form.Group controlId="formUsername">
          <Form.Control
            placeholder="Username"
            name="username"
            value={state.username}
          />
        </Form.Group>
        <Form.Group controlId="formPassword">
          <Form.Control
            type="password"
            placeholder="Password"
            name="password"
            value={state.password}
          />
        </Form.Group>
        <Form.Row>
          <Button variant="primary" type="submit">
            Log in
          </Button>
        </Form.Row>
        <Form.Check
          type="checkbox"
          label="Remember me"
          name="remember"
          value={!state.remember}
        />
      </Form>
    </Container>
  );
}

LoginComponent.propTypes = {
  handleChange: PropTypes.func,
  handleSubmit: PropTypes.func,
  state: PropTypes.shape({
    username: PropTypes.string,
    password: PropTypes.string,
    remember: PropTypes.bool,
  }),
}.isRequired;

export default LoginComponent;
