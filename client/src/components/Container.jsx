import React from 'react';

export const Container = ({ children }) => <div className="container px-4 mx-auto">{children}</div>;

export const Row = ({ children }) => <div className="flex flex-wrap -mx-4">{ children }</div>;

export const Col = ({ children }) => <div className="col relative w-0 px-4 max-w-full flex-grow">{ children }</div>;
