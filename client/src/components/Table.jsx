import React from 'react';

export const Table = ({ children }) => <table className="table w-full my-1 bg-transparent">{children}</table>;

export const TableHeader = ({ children }) => <th className="align-bottom p-3 text-left border-solid border-t border-b-2 border-gray-200">{children}</th>;

export const TableData = ({ children }) => <td className="align-bottom p-3 border-solid border-t border-gray-200">{children}</td>;
